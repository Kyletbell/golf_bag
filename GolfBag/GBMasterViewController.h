//
//  GBMasterViewController.h
//  GolfBag
//
//  Created by Kyle Bell on 6/4/14.
//  Copyright (c) 2014 Kyle Bell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBWebInterface.h"
#import "GBTableViewCell.h"
#import "GBIndicatorViewController.h"

@class GBDetailViewController;

@interface GBMasterViewController : UITableViewController <GBWebInterfaceDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) GBDetailViewController *detailViewController;
@property (strong, nonatomic) NSArray *golfClubs;


@end
