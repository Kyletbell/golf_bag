//
//  GBWebInterface.m
//  GolfBag
//
//  Created by Kyle Bell on 6/4/14.
//  Copyright (c) 2014 Kyle Bell. All rights reserved.
//

#define API_URL @"http://kyletbell.com/golf_clubs.php"

#import "GBWebInterface.h"

@implementation GBWebInterface

@synthesize delegate;

- (void)requestGolfClubList {
    
    NSURL *api_url = [[NSURL alloc] initWithString:API_URL];
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:api_url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        
        if (error) {
            NSLog(@"Error: %@",[error debugDescription]);
            [self.delegate requestGolfClubListDidFailWithError:error];
        } else {
            NSLog(@"Success: %@",[response debugDescription]);
            
            NSArray *response = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            
            
            [self.delegate requestGolfClubListDidDidSucceed: response];
        }
    }];
}

@end
