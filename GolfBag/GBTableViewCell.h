//
//  GBTableViewCell.h
//  GolfBag
//
//  Created by Kyle Bell on 6/4/14.
//  Copyright (c) 2014 Kyle Bell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GBTableViewCell : UITableViewCell

@property(nonatomic, retain) IBOutlet UILabel *clubLabel;
@property(nonatomic, retain) IBOutlet UILabel *priceLabel;

@end
