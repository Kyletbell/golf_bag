//
//  GBMasterViewController.m
//  GolfBag
//
//  Created by Kyle Bell on 6/4/14.
//  Copyright (c) 2014 Kyle Bell. All rights reserved.
//

#import "GBMasterViewController.h"

#import "GBDetailViewController.h"

@interface GBMasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation GBMasterViewController

@synthesize golfClubs;

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    
    self.detailViewController = (GBDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    GBWebInterface *interface = [[GBWebInterface alloc] init];
    [interface setDelegate:self];
    //[interface requestGolfClubList];
    
    _objects = [[NSMutableArray alloc] initWithArray:golfClubs];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GBTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSDictionary *object = _objects[indexPath.row];
    cell.clubLabel.text = [object objectForKey:@"name"];
    cell.priceLabel.text = [object objectForKey:@"price"];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        NSDictionary *object = _objects[indexPath.row];
        self.detailViewController.detailItem = object;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

- (void)requestGolfClubListDidDidSucceed:(NSArray *)response {
    _objects = [[NSMutableArray alloc] initWithArray:response copyItems:YES];
    
    self.title = @"Golf Clubs";
    [self.tableView reloadData];
}

- (void)requestGolfClubListDidFailWithError:(NSError *)error {
    NSString *err_message = [NSString stringWithFormat:@"ERROR: Golf Club Request Failed: %@",error];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Loading Data" message:err_message delegate:self cancelButtonTitle:@"Bummer" otherButtonTitles: nil];
    [alert setDelegate:self];
    [alert show];
}

@end
