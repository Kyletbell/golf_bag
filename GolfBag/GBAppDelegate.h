//
//  GBAppDelegate.h
//  GolfBag
//
//  Created by Kyle Bell on 6/4/14.
//  Copyright (c) 2014 Kyle Bell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
