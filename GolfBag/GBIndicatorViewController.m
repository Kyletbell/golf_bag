//
//  GBIndicatorViewController.m
//  GolfBag
//
//  Created by Kyle Bell on 6/4/14.
//  Copyright (c) 2014 Kyle Bell. All rights reserved.
//

#import "GBIndicatorViewController.h"

@interface GBIndicatorViewController ()

@end

@implementation GBIndicatorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    data = nil;
    
    GBWebInterface *interface = [[GBWebInterface alloc] init];
    [interface setDelegate:self];
    [interface requestGolfClubList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestGolfClubListDidDidSucceed:(NSArray *)response {
    data = response;
    [self performSegueWithIdentifier:@"Loaded" sender:self];
}

- (void)requestGolfClubListDidFailWithError:(NSError *)error {
    NSString *err_message = [NSString stringWithFormat:@"ERROR: Golf Club Request Failed: %@",error];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Loading Data" message:err_message delegate:self cancelButtonTitle:@"Bummer" otherButtonTitles: nil];
    [alert setDelegate:self];
    [alert show];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Loaded"]) {
        GBMasterViewController *masterViewController = [segue destinationViewController];
        [masterViewController setGolfClubs:data];
    }
}

@end
