//
//  GBWebInterface.h
//  GolfBag
//
//  Created by Kyle Bell on 6/4/14.
//  Copyright (c) 2014 Kyle Bell. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GBWebInterfaceDelegate <NSObject>

-(void)requestGolfClubListDidDidSucceed:(NSArray *)response;
-(void)requestGolfClubListDidFailWithError:(NSError *)error;

@end

@interface GBWebInterface : NSObject

@property (nonatomic, weak) id <GBWebInterfaceDelegate> delegate;

- (void)requestGolfClubList;

@end
